package com.pwc;

import java.io.*;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.*;

class Record {
    private final String node;
    private final String ip;
    private final String port;

    public Record(String node, String ip, String port) {
        this.node = node;
        this.ip = ip;
        this.port = port;
    }

    public String getNode() {
        return node;
    }

    public String getIp() {
        return ip;
    }

    public String getPort() {
        return port;
    }

    @Override
    public String toString() {
        return "Record{" +
                "node='" + node + '\'' +
                ", ip='" + ip + '\'' +
                ", port='" + port + '\'' +
                '}';
    }
}

class Node {
    private final String node;
    private final String ip;
    private final Set<String> ports;

    public Node(String node, String ip, Set<String> ports) {
        this.node = node;
        this.ip = ip;
        this.ports = ports;
    }

    public String getNode() {
        return node;
    }

    public String getIp() {
        return ip;
    }

    public Set<String> getPorts() {
        return ports;
    }
}

public class ApplicationEntryPoint {
    public static void persistOnFile(String filename, String data) throws Exception {
        try {

            String dirBase = System.getProperty("user.home") + File.separator + "plabooks";
            if (filename.contains("TIPP")) {
                dirBase = dirBase + File.separator + "ref";
            }

            File dirBaseFile = new File(dirBase, filename);
            FileWriter fileWriter = new FileWriter(dirBaseFile, false);
            try {
                fileWriter.write(data);
                fileWriter.close();
            } catch (IOException io) {
                System.out
                        .println("Cannot to write object on file: " + dirBaseFile + "IOException: " + io.getMessage());
            } finally {
                fileWriter.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void buildingPlaybook(Node node) {

        StringBuilder stringBuilder = new StringBuilder();
        String trackingNumber = UUID.randomUUID().toString().replace("-", "");

        String playbookFile = "playbook-" + trackingNumber + ".yml";
        String playbookRefFile = "TIPP" + trackingNumber + ".yml";

        stringBuilder.append("---\n");
        stringBuilder.append("  hosts: all\n");
        stringBuilder.append("    vars_files:\n");
        stringBuilder.append("    - ref/" + playbookRefFile);
        stringBuilder.append("\n  tasks:\n");
        stringBuilder.append("    - name: Checking Connectivity ");
        stringBuilder.append("from: ");
        stringBuilder.append("[ " + node.getIp() + " ]");
        stringBuilder.append(" to port: ");
        stringBuilder.append(node.getPorts());
        stringBuilder.append("\n");
        stringBuilder.append("    - wait_for:\n");
        stringBuilder.append("        host: \"{{ item.target_ip }}\"\n");
        stringBuilder.append("        port: \"{{ item.target_port }}\"\n");
        stringBuilder.append("        state: started\n");
        stringBuilder.append("        delay: 0\n");
        stringBuilder.append("        timeout: 1\n");
        stringBuilder.append("      loop: \"{{ reference_target_ip_address_port }}\"\n\n");
        try {
            persistOnFile(playbookFile, stringBuilder.toString());
            buildingPlaybookReferencePort(node, playbookRefFile);
        } catch (Exception io) {
            io.printStackTrace();
        }
    }

    public static void buildingPlaybookReferencePort(Node node, String playbookRefFile) throws Exception {

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("  reference_target_ipaddress_port:\n");

        for (String port : node.getPorts()) {
            stringBuilder.append("    - target_ip: ");
            stringBuilder.append(node.getIp());
            stringBuilder.append("\n");
            stringBuilder.append("    - target_port: ");
            stringBuilder.append(port);
            stringBuilder.append("\n\n");
        }
        persistOnFile(playbookRefFile, stringBuilder.toString());
    }

    public static List<Record> readCSV(String csvFile) {
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";
        List<Record> records = new ArrayList<>();

        try {

            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {

                String[] element = line.split(cvsSplitBy);
                Record record = new Record(element[0], element[1], element[2]);
                records.add(record);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return records;
    }

    public static void main(String[] args) throws URISyntaxException, IOException {
        List<Record> csvElements = new ArrayList<>();

        List<Record> csvParser = readCSV("servidores.csv");
        for (Record record : csvParser) {
            csvElements.add(new Record(record.getNode(), record.getIp(), record.getPort()));
        }

        Set<String> nodes = new HashSet<>();


        for (Record csvElement : csvElements) {
            nodes.add(csvElement.getNode());
        }

        Map<String, Set<String>> nodesIps = new HashMap<>();

        for (String node : nodes) {
            Set<String> ips = new HashSet<>();
            for (Record csvElement : csvElements) {
                if (csvElement.getNode().equals(node)) {
                    ips.add(csvElement.getIp());
                }
            }
            nodesIps.put(node, ips);
        }


        for (String group : nodesIps.keySet()) {
            Set<String> ips = nodesIps.get(group);
            for (String ip : ips) {
                Set<String> ports = new HashSet<>();

                for (Record csvElement : csvElements) {
                    if (csvElement.getNode().equals(group) && csvElement.getIp().equals(ip)) {
                        ports.add(csvElement.getPort());
                    }

                }
                Node node = new Node(group, ip, ports);
                buildingPlaybook(node);
            }
        }
    }

}
